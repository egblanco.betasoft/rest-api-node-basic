const { response } = require("express");
const User = require("../models/user");

const usersGET = (req, res = response) => {
  res.json({ msg: "GET - controller" });
};

const usersPOST = async (req, res = response) => {
  const { name, email, password, role, status, google } = req.body;
  const user = new User({ name, email, password, role, status, google });
  cleanUser = user.toJSON();
  await user.save();
  res.json({ msg: "POST", name, email });
};

const usersPUT = (req, res = response) => {
  const { name, age } = req.body;
  const { id } = req.params;
  const { status } = req.query;

  res.json({ msg: "PUT - controller", name, age, id, status });
};

module.exports = { usersGET, usersPUT, usersPOST };
