const { Router } = require("express");
const { usersGET, usersPUT, usersPOST } = require("../controllers/users");

const router = Router();

router.get("/", usersGET);

router.post("/", usersPOST);

router.put("/:id", usersPUT);

module.exports = router;
