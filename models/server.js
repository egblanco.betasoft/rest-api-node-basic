const express = require("express");
const cors = require("cors");
const { dbConnection } = require("../database/config");

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.usersRoute = "/api/users";

    //Database connection
    this.dbConnect();

    //Middlewares
    this.middlewares();

    //Routes
    this.routes();
  }

  async dbConnect() {
    await dbConnection();
  }

  middlewares() {
    //CORS
    this.app.use(cors());

    //Body Parser
    this.app.use(express.json());
  }

  routes() {
    this.app.use(this.usersRoute, require("../routes/user"));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("server is running on port " + this.port);
    });
  }
}

module.exports = Server;
